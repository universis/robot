import path from 'path';
import { TraceUtils, ApplicationService } from '@themost/common';
import Bree from 'bree';
import { JsonLogger } from './JsonLogger';


class BreeService extends ApplicationService {

    /**
     * @type {import('bree')}
     */
    rawService;

    constructor(app) {
        super(app);
        this.rawService = new Bree({
            root : path.resolve(__dirname, '../jobs'),
            logger: new JsonLogger(),
        });
    }

    get jobs() {
        return this.rawService.config.jobs;
    }

    /**
     * @param {string=} name
     * @returns {Promise<void>}
     */
    start(name) {
        return this.rawService.start(name);
    }

    /**
     * @param {string=} name
     * @returns {Promise<void>}
     */
    stop(name) {
        return this.rawService.stop(name);
    }

    /**
     * Loads jobs from the given configuration file
     * @param {string} configFile
     */
    async load(configFile) {
        const configPath = path.resolve(process.cwd(), configFile);
        const jobs = require(configPath);
        if (Array.isArray(jobs) === false) {
            TraceUtils.warn('Invalid configuration file. Expected an array of jobs.');
        } else {
            await this.rawService.init();
            // get configuration file directory
            const configDir = path.dirname(path.resolve(configFile));
            await Promise.sequence(jobs.map((job) => {
                return () => {
                    if (Object.prototype.hasOwnProperty.call(job, 'path')) {
                        if (job.path.startsWith('.')) {
                            job.path = path.resolve(configDir, job.path);
                        }
                    }
                    return this.rawService.add(job);
                }
            }));
        }
    }

    /**
     * @returns {Promise<void>}
     */
    init() {
        return this.rawService.init();
    }

    /**
     * @returns {Promise<void>}
     * @param {*} job 
     */
    add(job) {
        return this.rawService.add(job);
    }

    /**
     * @returns {Promise<void>}
     * @param {string=} name
     */
    run(name) {
        return this.rawService.run(name);
    }

    /**
     * @param {string} name 
     * @returns {Promise<void>}
     */
    remove(name) {
        return this.rawService.remove(name);
    }
}

module.exports = {
    BreeService
};
