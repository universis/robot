import express from 'express';
import path from 'path';
import Twig from "twig";
import { ExpressDataApplication, dateReviver } from '@themost/express';
import indexRouter from './routes/index';
import { BreeService } from './services/BreeService';
import {HttpNotFoundError} from "@themost/common";

/**
 * @returns {import('express').ErrorRequestHandler}
 */
function errorHandler() {
  // eslint-disable-next-line no-unused-vars
  return function (err, req, res, next) {
    // if requests accepts json
    // https://expressjs.com/en/api.html#req.accepts
    let status = err.status || err.statusCode || 500;
    if (req.get('accept') === 'application/json') {
      req.context && req.context.trace && req.context.trace.error('ERROR', err);
      // do not render html error and send error as json
      res.status(status);
      // serialize error
      let error = {};
      Object.getOwnPropertyNames(err).forEach(key => {
        if (process.env.NODE_ENV !== 'development' && key==='stack') {
          return;
        }
        error[key] = err[key];
      });
      const proto = Object.getPrototypeOf(err);
      if (proto && proto.constructor.name) {
        error['type'] = proto.constructor.name;
      }
      return res.json(error);
    }
    // otherwise send html content
    // set locals, only providing error in development
    const locals = {
        title: 'Error',
        message: err.message,
        status: status,
        error: req.app.get('env') === 'development' ? err : {}
    }
    res.status(status);
    res.render('error', locals);
  }
}

function getApplication() {
  /**
* @name Request#context
* @description Gets an instance of ExpressDataContext class which is going to be used for data operations
* @type {import('@themost/express').ExpressDataContext}
*/
  /**
   * @name express.Request#context
   * @description Gets an instance of ExpressDataContext class which is going to be used for data operations
   * @type {import('@themost/express').ExpressDataContext}
   */

  /**
   * Initialize express application
   * @type {Express}
   */
  let app = express();

  // view engine setup
  app.set('view engine', 'twig');
  app.set("view options", { layout: false });
  app.set('views', path.join(__dirname, 'views'));

  app.use(express.json({
    reviver: dateReviver
  }));
  app.use(express.urlencoded({ extended: false }));

  // @themost/data data application setup
  const dataApplication = new ExpressDataApplication(path.resolve(__dirname, 'config'));
  // hold data application
  app.set('ExpressDataApplication', dataApplication);
  // use data middleware (register req.context)
  app.use(dataApplication.middleware());

  dataApplication.useService(BreeService);
  dataApplication.getService(BreeService).start();
  // use static content
  app.use(express.static(path.join(process.cwd(), 'public')));

  app.use('/', indexRouter);

  app.use((req, res, next) => {
    next(new HttpNotFoundError());
  });

  // error handler
  app.use(errorHandler());
  return app
}

export {
  getApplication
}
