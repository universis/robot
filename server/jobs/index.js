//get configuration source
import path from 'path';
import fs from 'fs';
import {TraceUtils} from "@themost/common";
let CONFIG = {};
let configSourcePath;
try {
    let env = 'production';
    if (process && process.env) {
        env = process.env['NODE_ENV'] || 'production';
    }
    configSourcePath = path.resolve(__dirname,'../config/jobs', 'jobs.' + env + '.json');
    if (!fs.existsSync(configSourcePath)) {
        // load default jobs configuration file
        TraceUtils.warn(`Jobs configuration for ${env} not found.`);
        configSourcePath = path.resolve(__dirname,'../config/jobs/jobs.json');
    }
    CONFIG = require(configSourcePath);
}
catch (err) {
    TraceUtils.error('Error loading jobs configuration file.');
    TraceUtils.error(err);
}
module.exports = CONFIG.jobs;
