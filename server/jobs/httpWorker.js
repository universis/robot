import {TraceUtils, ConfigurationBase, HttpError, DataError} from "@themost/common";
import {URL} from "url";
import path from "path";
const request  = require('superagent');
const { workerData } = require('worker_threads');

async function main() {
    if (workerData.job.params && workerData.job.params.keychain) {
        const configuration = new ConfigurationBase(path.resolve(__dirname, '../config'));
        const keychain = configuration.settings.keychain || [];
        // validate keychain
        if (!(Array.isArray(keychain) && keychain.length)) {
            throw new DataError('E_KEYCHAIN_CONFIG', 'The process cannot continue due to invalid keychain configuration.'
            );
        }
        // then try to find the specified identifier
        const keychainItem = keychain.find(
            (item) => item.identifier === workerData.job.params.keychain);
        if (keychainItem == null) {
            throw new Error(
                'E_KEYCHAIN_ITEM',
                'The specified keychain identifier cannot be found inside the keychain configuration',
                null,
                'ServiceProvider',
                'authenticator'
            );
        }
        const form = {
            'grant_type': keychainItem.grant_type,
            'scope': keychainItem.scope,
            'username': keychainItem.username,
            'password': keychainItem.password,
            'client_id': keychainItem.client_id,
            'client_secret': keychainItem.client_secret
        };
        const response = await authorize(keychainItem.tokenURL, form);
        const url = new URL(workerData.job.params.url, keychainItem.url).toString();
        const method = workerData.job.params.method;
        const data = workerData.job.params.data;

        return request(method, url)
            .set('Authorization', 'Bearer ' + response.access_token)
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/json')
            // eslint-disable-next-line no-unused-vars
            .send(data).then(response => {
                return Promise.resolve();
            });
    }
}

function authorize(tokenUrl, authorizeUser) {
    return new Promise((resolve, reject)=> {
        return request.post(new URL('', tokenUrl))
            .type('form')
            .send(authorizeUser).then(function (response) {
                if (response && response.error) {
                    if (response.clientError) {
                        return reject(new HttpError(response.error.status));
                    }
                    return reject(response.error);
                }
                return resolve(response.body);
            }).catch(err=>{
                return reject(err);
            });
    });
}


(async () => {
    await main()
})().then(() => {
    process.exit();
}).catch((err) => {
    TraceUtils.error(err);
    process.exit(-1);
});


