import globals from "globals";
import babelParser from "@babel/eslint-parser";
import path from "node:path";
import { fileURLToPath } from "node:url";
import js from "@eslint/js";
import { FlatCompat } from "@eslint/eslintrc";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
    baseDirectory: __dirname,
    recommendedConfig: js.configs.recommended,
    allConfig: js.configs.all
});

export default [{
    ignores: ["**/*.ts", "**/*.tsx"],
}, ...compat.extends("eslint:recommended"), {
    languageOptions: {
        globals: {
            ...Object.fromEntries(Object.entries(globals.browser).map(([key]) => [key, "off"])),
            ...globals.node,
            ...Object.fromEntries(Object.entries(globals.jquery).map(([key]) => [key, "off"])),
            describe: false,
            fdescribe: false,
            IntlPolyfill: false,
            before: false,
            after: false,
            beforeAll: false,
            beforeEach: false,
            afterAll: false,
            afterEach: false,
            it: false,
            fit: false,
            angular: false,
            jQuery: false,
            jasmine: false,
            expect: false,
            expectAsync: false,
            spyOn: false,
            spyOnProperty: false,
        },

        parser: babelParser,
    },

    rules: {
        "no-console": "warn",
        "no-invalid-this": "warn",
        "no-undef": "error",
        "no-unused-vars": "warn",
        "no-var": ["error"],
        strict: [2, "never"],
    },
}];