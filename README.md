# @universis/robot

A task runner for Universis project based on [bree](https://github.com/breejs/bree) task scheduler

## Installation

Start an instance of `@universal/robot` by executing the following command:

```bash
npx @universis/robot
```

This installs the package and starts the task runner with the default configuration. `@universis/robot` starts its own application server and listens for incoming requests.

Use the `--port` option to specify a different port.  The default port is `5445`. 

```bash
npx @universis/robot --port 6000
```

Use the `--host` option to specify the host on which the application server listens for incoming requests. The default host is `localhost`

```bash
npx @universis/robot --port 6000 --host "0.0.0.0"
```

Use the `--config` option to specify the path to the jobs configuration file. 

```bash
npx @universis/robot --config /path/to/config.json
```

## Configuation

The configuration file is a JSON file that contains the jobs to be scheduled. The following is an example of a configuration file:

```json
{
  "jobs": [
    {
      "name": "job1",
      "interval": "1 hour",
      "path": "/path/to/job1.js"
    },
    {
      "name": "job2",
      "timeout": "8 hours",
      "path": "/path/to/job2.js"
    }
  ]
}
```

Read more about bree job scheduling [here](https://github.com/breejs/bree?tab=readme-ov-file#job-options)

## Examples

[Universis robot example](https://gitlab.com/universis/universis-robot-example) is a simple project of how to use `@universis/robot` to schedule jobs. 
It contains only one job that gets stats for students grouped by their student status.


## Development

Clone the repository and install the dependencies:

```bash
git clone https://gitlab.com/universis/robot.git
cd robot
npm install
```

`@universis/robot` uses jasmine for testing. Run the tests with the following command:

```bash
npm test
```

Start the application with the following command:

```bash
npm start
```
