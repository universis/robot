import { getApplication } from '../server/app';
import { BreeService } from '../server/services/BreeService';
import path from 'path';
import fs from 'fs';

describe('BreeService', () => {
    /**
     * @type {import('@themost/express').ExpressDataContext}
     */
    let context;
    beforeAll(() => {
        /**
         * @type {import('express').Application}
         */
        const container = getApplication();
        /**
         * @type {import('@themost/express').ExpressDataApplication}
         */
        const app = container.get('ExpressDataApplication');
        context = app.createContext();
    });
    afterAll(async () => {
        await context.finalizeAsync()
    });

    it('should get service', () => {
        const service = context.application.getService(BreeService);
        expect(service).toBeTruthy();
        expect(service.rawService).toBeTruthy();
    });

    it('should add job', async () => {
        /**
         * @type {BreeService}
         */
        const service = context.application.getService(BreeService);
        await service.init();
        await service.add({
            timeout: '1 second',
            name: 'test',
            path:  path.resolve(__dirname, 'testJob.js')
        });
        expect(service.jobs).toBeTruthy();
        expect(service.jobs.length).toBe(1);
        fs.mkdirSync(path.resolve(process.cwd(), '.tmp'), { recursive: true });
        const logFile = path.resolve(process.cwd(), '.tmp', 'testJob.log');
        if (fs.existsSync(logFile)) {
            fs.unlinkSync(logFile);  
        }
        await service.start('test');
        await new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, 4000);
        });
        await service.remove('test');
        expect(fs.existsSync(logFile));

    });

    it('should remove job', async () => {
        /**
         * @type {BreeService}
         */
        const service = context.application.getService(BreeService);
        await service.init();
        expect(service.jobs.length).toBe(0);
        await service.add({
            timeout: '1 second',
            name: 'test',
            path:  path.resolve(__dirname, 'testJob.js')
        });
        expect(service.jobs.length).toBe(1);
        await service.remove('test');
        expect(service.jobs.length).toBe(0);

    });


});